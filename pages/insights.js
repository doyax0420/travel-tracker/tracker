import { useState, useEffect } from 'react';
import moment from 'moment';
import { Tabs, Tab } from 'react-bootstrap';
import MonthlyChart from '../components/MonthlyChart';

export default function Insights(){
	const [distances, setDistances] = useState([]);
	const [duration, setDuration] = useState(0);
	const [amount, setAmount] = useState(0);

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, { 
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			console.log(data.travels)
			if(data.travels.length > 0){

				let monthlyDistance = [ 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 ]
				let monthlyDuration = [ 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 ]
				let monthlyAmount = [ 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 ]

				let durations = 0;

				let amounts = 0;

				data.travels.forEach(travel => {

					const index = moment(travel.date).month();

					monthlyDistance[index] += (travel.distance/1000);
					//console.log(Math.round(travel.duration/60))
					monthlyDuration[index] += Math.round(travel.duration/60); 
					
					if(travel.charge){
						monthlyAmount[index] += (amounts += travel.charge.amount);
					}
					
				})
				//once all travel record have been loop through, set the state to their corresponding array
				setDistances(monthlyDistance)
								
				setDuration(monthlyDuration)
				
				setAmount(monthlyAmount)

			}

		})
	}, []);

	return(
		<Tabs defaultActiveKey="distances" id="monthlyFigures">
			<Tab eventKey="distances" title="Monthly Distance Travelled">
				<MonthlyChart 
					figuresArray={distances}
					label="Monthly total in kilometers"
				/>
			</Tab>
			<Tab eventKey="duration" title="Monthly Duration">
				<MonthlyChart 
					figuresArray={duration}
					label="Monthly duration in minutes"
				/>
			</Tab>
			<Tab eventKey="amount" title="Monthly Amount Spent">
				<MonthlyChart 
					figuresArray={amount}
					label="Monthly amount in PHP"
				/>
			</Tab>
		</Tabs>
	)
}