import React from 'react'
import ReactDOM from 'react-dom';
const PayPalBtn = paypal.Buttons.driver("react", { React, ReactDOM });
import StreetNavigation from './StreetNavigation';

export default function PayPalButton({ amount, completeBooking }){
	const [orderId, setOrderId] = ('');

	const createOrder = (data, actions) => {
		return actions.order.create({
			purchase_units: [
				{
					amount: {
						value: amount
					}
				}
			]
		})
	}

	const onApprove = (data, actions) => {
		//console.log(data.orderID)

		if(data.orderID  !== null ){
			//Calls the completeBooking function to reset the travel details and record the boooking in our database
			completeBooking(data.orderID);
		}

		//Capture this transaction in your paypal account dashboard
		return actions.order.capture()
	}

	return(
		<PayPalBtn 
			createOrder={(data, actions) => createOrder(data, actions)}
			onApprove = {(data, actions) => onApprove(data, actions)}
		/>
	)
}